/* global QUnit */
QUnit.config.autostart = false;

sap.ui.getCore().attachInit(function () {
	"use strict";

	sap.ui.require([
		"ns_testGit/mod_testGit/test/unit/AllTests"
	], function () {
		QUnit.start();
	});
});